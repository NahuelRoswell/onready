import dominio.*;

import java.util.ArrayList;
import java.util.List;

public class Main {


    public static void main(String[] args) {
        Comparador comparador = new Comparador();
        Buscador buscador = new Buscador();

        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        vehiculos.add(new Auto("Peugeot","206",200000,"4"));
        vehiculos.add(new Moto("Honda","Titan",60000,"125"));
        vehiculos.add(new Auto("Peugeot","208",250000,"5"));
        vehiculos.add(new Moto("Yamaha","YBR",80500.50,"160"));

        solver(comparador, buscador, vehiculos);

    }

    private static void solver(Comparador comparador, Buscador buscador, ArrayList<Vehiculo> vehiculos) {
        comparador.mostrarVehiculos(vehiculos);

        Vehiculo vehiculoMasCaro = comparador.buscarMasCaro(vehiculos);
        Vehiculo vehiculoMasBarato = comparador.buscarMasBarato(vehiculos);
        List<Vehiculo> vehiculosConLetraY = buscador.buscarPorLetra(vehiculos, "Y");

        System.out.println("Vehiculo más caro: " +vehiculoMasCaro.getMarca() +" " +vehiculoMasCaro.getModelo());
        System.out.println("Vehiculo más barato: " +vehiculoMasBarato.getMarca() +" "+vehiculoMasBarato.getModelo());
        System.out.print("Vehiculo que contiene la letra 'Y': " );

        for (Vehiculo vehiculo : vehiculosConLetraY) {
            System.out.println(vehiculo.getMarca() +" "
                    + vehiculo.getModelo() +" $"
                    +vehiculo.convertirAPrecio()
            );
        }

        System.out.println("=============================");
        List<Vehiculo> vehiculosOrdenadosDeMayorAMenor = comparador.ordenarDeMayorAMenor(vehiculos);
        for (Vehiculo vehiculo : vehiculosOrdenadosDeMayorAMenor) {
            System.out.println(vehiculo.getMarca() +" " +vehiculo.getModelo());

        }
    }
}
