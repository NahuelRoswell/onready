package dominio;

import java.util.List;

public interface ComparadorPrecios {

    Vehiculo buscarMasCaro(List<Vehiculo> vehiculos);

    Vehiculo buscarMasBarato(List<Vehiculo> vehiculos);

    List<Vehiculo> ordenarDeMayorAMenor(List<Vehiculo> vehiculos);

    void mostrarVehiculos(List<Vehiculo> vehiculos);

}
