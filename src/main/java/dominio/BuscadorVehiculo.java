package dominio;

import java.util.List;

public interface BuscadorVehiculo {

    List<Vehiculo> buscarPorLetra(List<Vehiculo> vehiculos, String letra);
}
