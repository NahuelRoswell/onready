package dominio;

public abstract class Vehiculo implements Comparable<Vehiculo> {
    private String marca, modelo;
    private double precio;

    public Vehiculo(String marca, String modelo, double precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public double getPrecio() {
        return precio;
    }

    public String convertirAPrecio() {
        int contador = 0;
        String precioOriginal = "" + precio;
        String precioAMostrar = "";

        for (int indice = precioOriginal.length() - 1; indice >= 0; indice--) {
            if (contador == 3) {
                precioAMostrar = "." + precioAMostrar;
                contador = 0;
            }

            if (precioOriginal.charAt(indice) == '.') {
                precioAMostrar = "," + precioAMostrar;
                contador = 0;
            } else {
                precioAMostrar = precioOriginal.charAt(indice) + precioAMostrar;
                contador++;
            }
        }


        return chequearDecimales(precioAMostrar);
    }

    private String chequearDecimales(String precioAMostrar) {
        int indexDeComa = precioAMostrar.indexOf(",")+1;

        if (precioAMostrar.length() - indexDeComa == 1){
            precioAMostrar = precioAMostrar + "0";
        }

        return precioAMostrar;
    }


    @Override
    public int compareTo(Vehiculo vehiculo) {
        if (this.precio < vehiculo.precio)
            return -1;
        if (this.precio > vehiculo.precio)
            return 1;
        return 0;
    }

}
