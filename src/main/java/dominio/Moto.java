package dominio;

public class Moto extends Vehiculo{
    private String cilindrada;

    public Moto(String marca, String modelo, double precio, String cilindrada) {
        super(marca, modelo, precio);
        this.cilindrada = cilindrada;
    }

    @Override
    public String toString() {
        return "Marca: " + getMarca() +" // "
                + "Modelo: " + getModelo() +" // "
                + "Cilindrada: " + cilindrada +"c // "
                + "Precio: $" + convertirAPrecio();
    }
}
