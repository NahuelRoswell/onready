package dominio;

import java.util.*;

public class Comparador implements ComparadorPrecios{

    @Override
    public Vehiculo buscarMasCaro(List<Vehiculo> vehiculos) {
        if (vehiculos.size() == 0)
            return null;

        Collections.sort(vehiculos);

        return vehiculos.get(vehiculos.size()-1);
    }

    @Override
    public Vehiculo buscarMasBarato(List<Vehiculo> vehiculos) {
        if (vehiculos.size() == 0)
            return null;

        Collections.sort(vehiculos);

        return vehiculos.get(0);
    }

    @Override
    public List<Vehiculo> ordenarDeMayorAMenor(List<Vehiculo> vehiculos) {
        Collections.sort(vehiculos);

        Collections.reverse(vehiculos);

        return vehiculos;
    }


    @Override
    public void mostrarVehiculos(List<Vehiculo> vehiculos) {
        for (Vehiculo vehiculo : vehiculos) {
            System.out.println(vehiculo);
        }
        System.out.println("=============================");
    }


}
