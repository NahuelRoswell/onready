package dominio;

import java.util.ArrayList;
import java.util.List;

public class Buscador implements BuscadorVehiculo {

    @Override
    public List<Vehiculo> buscarPorLetra(List<Vehiculo> listaVehiculos, String letra) {
        ArrayList<Vehiculo> retorno = new ArrayList<Vehiculo>();

        if (listaVehiculos.size() == 0)
            return retorno;

        agregarAListaSiContieneLetra(listaVehiculos, letra, retorno);

        return retorno;
    }

    private void agregarAListaSiContieneLetra(List<Vehiculo> vehiculos, String letra, ArrayList<Vehiculo> retorno) {
        for (Vehiculo vehiculo : vehiculos)
            if (contieneLetra(vehiculo, letra))
                retorno.add(vehiculo);
    }

    private boolean contieneLetra(Vehiculo vehiculo, String letra) {
        String modelo = vehiculo.getModelo();

        return modelo.contains(letra);
    }

}
