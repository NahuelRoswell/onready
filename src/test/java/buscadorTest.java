import dominio.Auto;
import dominio.Buscador;
import dominio.Moto;
import dominio.Vehiculo;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class buscadorTest {
    Buscador buscador;
    ArrayList<Vehiculo> vehiculos;

    @Before
    public void inicializar() {
        buscador = new Buscador();
        vehiculos = new ArrayList<Vehiculo>();

        vehiculos.add(new Auto("Peugeot", "206", 200000, "4"));
        vehiculos.add(new Moto("Honda", "Titan", 60000, "125"));
        vehiculos.add(new Auto("Peugeot", "208", 250000, "5"));
        vehiculos.add(new Moto("Yamaha", "YBR", 80500.50, "160"));
    }

    @Test
    public void buscarPorLetra_conListaVacia_retornaListaVacia() {
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        assertEquals(0, buscador.buscarPorLetra(vehiculos, "s").size());
    }

    @Test
    public void buscarPorLetra_conLetraNoExistente_retornaListaVacia(){
        List<Vehiculo> listaVacia = buscador.buscarPorLetra(vehiculos, "w");

        assertEquals(0,listaVacia.size());
    }

    @Test
    public void buscarPorLetra_ConLetraExistente_retornaYBR(){
        Vehiculo moto = buscador.buscarPorLetra(vehiculos, "Y").get(0);

        assertEquals("YBR",moto.getModelo());
    }

    @Test
    public void buscarPorLetra_conMasDeUnaCoincidencia_retornaListaCon2Vehiculos(){
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();
        vehiculos.add(new Auto("Peugeot", "206", 200000, "4"));
        vehiculos.add(new Moto("Honda", "Z", 60000, "125"));
        vehiculos.add(new Auto("Peugeot", "Z1", 250000, "5"));
        vehiculos.add(new Moto("Yamaha", "YBR", 80500.50, "160"));

        List<Vehiculo> vehiculosQueContienenZ = buscador.buscarPorLetra(vehiculos, "Z");

        assertEquals(2, vehiculosQueContienenZ.size());
        assertEquals("Z", vehiculosQueContienenZ.get(0).getModelo());
        assertEquals("Z1", vehiculosQueContienenZ.get(1).getModelo());
    }


}
