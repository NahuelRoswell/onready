import dominio.Auto;
import dominio.Comparador;
import dominio.Moto;
import dominio.Vehiculo;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class ComparadorTest {
    Comparador comparador;
    ArrayList<Vehiculo> vehiculos;

    @Before
    public void inicializar() {
        comparador = new Comparador();

        vehiculos = new ArrayList<Vehiculo>();
        vehiculos.add(new Auto("Peugeot","206",200000,"4"));
        vehiculos.add(new Moto("Honda","Titan",60000,"125"));
        vehiculos.add(new Auto("Peugeot","208",250000,"5"));
        vehiculos.add(new Moto("Yamaha","YBR",80500.50,"160"));
    }

    @Test
    public void buscarMasCaro_ConValoresValidos_retornaYamahaR1(){
        Vehiculo vehiculoMasCaro = comparador.buscarMasCaro(vehiculos);

        assertEquals("208",vehiculoMasCaro.getModelo());
    }

    @Test
    public void buscarMasCaro_ConUnSoloELemento_retornaEseElemento(){
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        vehiculos.add(new Auto("Fiat","600",200,"3"));
        Vehiculo vehiculoMasCaro = comparador.buscarMasCaro(vehiculos);

        assertEquals("600", vehiculoMasCaro.getModelo());
    }

    @Test
    public void buscarMasCaro_ConListaVacia_retornaNull(){
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        Vehiculo vehiculoMasCaro = comparador.buscarMasCaro(vehiculos);

        assertNull(vehiculoMasCaro);
    }




    @Test
    public void buscarMasBarato_ConValoresValidos_retornaYamahaR1(){
        Vehiculo mostrarMasBarato = comparador.buscarMasBarato(vehiculos);

        assertEquals("Titan",mostrarMasBarato.getModelo());
    }

    @Test
    public void buscarMasbarato_ConUnSoloELemento_retornaEseElemento(){
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        vehiculos.add(new Auto("Fiat","600",200,"3"));
        Vehiculo vehiculoMasBarato = comparador.buscarMasBarato(vehiculos);

        assertEquals("600", vehiculoMasBarato.getModelo());
    }

    @Test
    public void buscarMasBarato_ConListaVacia_retornaNull(){
        ArrayList<Vehiculo> vehiculos = new ArrayList<Vehiculo>();

        Vehiculo vehiculoMasBarato = comparador.buscarMasBarato(vehiculos);

        assertNull(vehiculoMasBarato);
    }


}
